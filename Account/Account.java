package Account;

import java.math.BigInteger;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Account implements java.io.Serializable{
    private double currentBalance;
    private BigInteger accountNumber;
    private Date openingDate;
    private String valuta;


    public Account(){

    }
    public Account(BigInteger accountNumber,double currentBalance,Date openingDate,String valuta){
            this.currentBalance=currentBalance;
            this.accountNumber=accountNumber;
            this.openingDate=openingDate;
            this.valuta=valuta;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public BigInteger getAccountNumber() {
        return accountNumber;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public String getValuta() {
        return valuta;
    }

    public void setValuta(String valuta) {
        this.valuta = valuta;
    }

    public void setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public void setAccountNumber(BigInteger accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(accountNumber, account.accountNumber);
    }
    @Override
    public int hashCode() {

        return Objects.hash(accountNumber);
    }
    @Override
    public String toString() {
        Format formatter = new SimpleDateFormat("dd/MM/yy");
        String string = formatter.format(openingDate);
        return
                "currentBalance=" + currentBalance +
                ", accountNumber=" + accountNumber +
                ", openingDate=" + string +
                ", valuta='" + valuta + '\'' ;
    }
}
