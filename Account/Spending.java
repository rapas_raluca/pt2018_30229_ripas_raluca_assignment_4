package Account;

import java.math.BigInteger;
import java.util.Date;

public class Spending extends Account implements java.io.Serializable{

    public Spending(BigInteger accountNumber, double currentBalance, Date openingDate, String valuta){
        super(accountNumber,currentBalance,openingDate,valuta);
    }

    public Spending(){

    }

    public void withdraw(double withdrawBalance){
        double currentBalance=getCurrentBalance();
        if(currentBalance>=withdrawBalance){
            currentBalance=currentBalance-withdrawBalance;
            setCurrentBalance(currentBalance);
           System.out.println("A fost extrasa suma de "+withdrawBalance+" din contul "+getAccountNumber()+". Suma ramasa este: "+getCurrentBalance());
        }
        else{
            System.out.println("Sold insuficient");
        }
    }

    public void add(double addBalance){
        setCurrentBalance(getCurrentBalance()+addBalance);
    }

    public String toString(){
        return "Spending "+super.toString();
    }
}
