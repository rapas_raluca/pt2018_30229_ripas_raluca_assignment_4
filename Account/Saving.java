package Account;

import java.lang.reflect.GenericArrayType;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Saving extends Account implements java.io.Serializable {

      private double interestRate;
      private int ok=0;

      public Saving(){

      }

      public Saving(double interestRate, BigInteger accountNumber, double currentBalance, Date openingDate, String valuta){
          super(accountNumber,currentBalance,openingDate,valuta);
          this.interestRate=interestRate;
          this.ok=1;
      }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    private int calculateNumberOfDays(){
          //setCurrentBalance(getCurrentBalance()+getCurrentBalance()*interestRate/100);
        DateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
        Date today = new Date();
        return daysBetween( today, getOpeningDate());
    }

    private int daysBetween(Date d1, Date d2){
        int b= (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
        if(b<0)
            return -b;
        else
            return b;
    }

    public double getPlus(){
        double currentBalance=getCurrentBalance();
        double rate=calculateNumberOfDays()/30.0;
        //System.out.println("r "+rate);
        return currentBalance*interestRate/100*rate;
    }
    public double withdraw(){
          double balance=getPlus()+getCurrentBalance();
          setCurrentBalance(0);
          this.ok=0;
          return balance;
    }

    public int getOk(){
          return this.ok;
    }

    public void add(double balance){
          setCurrentBalance(balance);
          this.ok=1;
    }
    @Override
    public String toString() {
        return "Saving Account{"+ super.toString()+ ','+
                " interestRate=" + interestRate +
                '}';
    }
}
