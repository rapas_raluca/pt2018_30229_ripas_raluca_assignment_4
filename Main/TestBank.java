package Main;

import Account.Account;
import Account.Spending;
import Account.Saving;
import Bank.Bank;
import Person.Person;
import org.junit.Assert;
import org.junit.jupiter.api.Test;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class TestBank {
    HashMap<BigInteger,Person> persons=new HashMap<>();
    HashMap<BigInteger,ArrayList<Account>> accounts= new HashMap<>();
    Bank bank= new Bank(accounts,persons);
    Person p1= new Person(new BigInteger("1851021345131"),"Ferdean","Ciprian","str. Muresului nr.50","0745655611","ciprif@yahoo.com");
    Person p2=new Person(new BigInteger("1920617149053"),"Stan","Andrei","str. Observatorului nr.20","0747739281","stanley@yahoo.com");
    Person p3= new Person(new BigInteger("1931011351347"),"Bucnari","Cristina","str. Campului nr.12","0755901670","bucnaric@gmail.com");
    Person p4= new Person(new BigInteger("1860525171600"),"Rotar","Mihai","str. Florilor nr.20","0766381923","email=mr2010@gamil.com");
    Saving s= new Saving(2,new BigInteger("4829320821"),1000,new Date("01/02/2018"),"RON");
    Saving s1= new Saving(1,new BigInteger("4829320900"),1000,new Date("03/22/2018"),"EUR");
    Spending s2= new Spending(new BigInteger("4829320910"),1000,new Date("02/07/2018"),"EUR");
    Spending s3= new Spending(new BigInteger("4299010001"),2000,new Date("04/03/2017"),"RON");
    Spending s4= new Spending(new BigInteger("4849282932"),6500,new Date("04/08/2017"),"EUR");
    Saving s5= new Saving(0.5,new BigInteger("2983488492"),8000,new Date("01/10/1018"),"RON");

    @Test
    public void addNewPerson() throws AssertionError{
        persons.put(p1.getCNP(),p1);
        Assert.assertTrue(  bank.findPerson(p1.getCNP())!=null);
    }

    @Test
    public void addNewSaving() throws AssertionError{
        persons.put(p1.getCNP(),p1);
        bank.addNewSaving(s,p1.getCNP());
        Assert.assertTrue(bank.findSaving(s)!=null);
    }

    @Test
    public void deleteSaving() throws AssertionError{
        persons.put(p1.getCNP(),p1);
        bank.addNewSaving(s,p1.getCNP());
        bank.deleteSaving(s,p1.getCNP());
        Assert.assertTrue(bank.findSaving(s)==null);
    }

    @Test
    public void addSpending() throws AssertionError{
        persons.put(p1.getCNP(),p1);
        bank.addNewSpending(s2,p1.getCNP());
        Assert.assertTrue(bank.findSpending(s2)!=null);
    }

    @Test
    public void deleteSpending() throws AssertionError{
        persons.put(p1.getCNP(),p1);
        bank.addNewSpending(s2,p1.getCNP());
        bank.deleteSpending(s2,p1.getCNP());
        Assert.assertTrue(bank.findSpending(s2)==null);
    }

    @Test
    public void addMoneySaving() throws AssertionError{
        persons.put(p1.getCNP(),p1);
        bank.addNewSaving(s,p1.getCNP());
        bank.readAccount(s,1,p1.getCNP());
        double cb=s.getCurrentBalance();
        double b=200.0;
        bank.writeAccount(s,200,p1.getCNP());
        Assert.assertTrue(s.getCurrentBalance()==b);
    }

    @Test
    public void addMoneySpending() throws AssertionError{
        persons.put(p1.getCNP(),p1);
        bank.addNewSpending(s2,p1.getCNP());
        double cb=s2.getCurrentBalance();
        bank.writeAccount(s2,200,p1.getCNP());
        Assert.assertTrue(cb+200==s2.getCurrentBalance());

    }

    @Test
    public void withdrawSaving() throws AssertionError{
        persons.put(p1.getCNP(),p1);
        bank.addNewSaving(s,p1.getCNP());
        bank.readAccount(s,1,p1.getCNP());
        Assert.assertTrue(s.getCurrentBalance()==0);
    }

    @Test
    public void withdrawSpending() throws AssertionError{
        persons.put(p1.getCNP(),p1);
        bank.addNewSpending(s2,p1.getCNP());
        double cb=s2.getCurrentBalance();
        bank.readAccount(s2,200,p1.getCNP());
        Assert.assertTrue(cb-200==s2.getCurrentBalance());
    }
}
