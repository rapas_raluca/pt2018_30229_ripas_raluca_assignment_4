package Interface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.math.BigInteger;

import Account.Account;
import Account.Saving;
import Account.Spending;
import Person.Person;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import Bank.Bank;
import org.w3c.dom.events.MouseEvent;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Controller {

    private View v;
    private Bank b;
    public Controller(View v,Bank b){
        this.v=v;
        this.b=b;
        v.displayCButtonListener(new displayCButtonListener());
        v.insertCButtonListener(new insertCButtonListener());
        v.deleteCButtonListener(new deleteCButtonListener());
        v.updateCButtonListener(new updateCButtonListener());

        v.displaySavButtonListener(new displaySavButtonListener());
        v.insertSavButtonListener(new insertSavButtonListener());
        v.updateSavButtonListener(new updateSavButtonListener());
        v.deleteSavButtonListener(new deleteSavButtonListener());

        v.displaySpendButtonListener(new displaySpendButtonListener());
        v.insertSpendButtonListener(new insertSpendCButtonListener());
        v.updateSpendButtonListener(new updateSpendButtonListener());
        v.deleteSpendButtonListener(new deleteSpendButtonListener());

        v.addSpendButtonListener(new addSpendButtonListener());
        v.addSavButtonListener(new addSavButtonListener());
        v.withdrawSavButtonListener(new withdrawSavButtonListener());
        v.withdrawSpendButtonListener(new withdrawSpendButtonListener());
      //  v.pTableMouseListener(new pTableMouseListener());


    }
    private class displayCButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            showOnPersonTable();
        }
    }
    private class insertCButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            Person p=new Person(new BigInteger(v.getCNPText().getText()),v.getNumeCText().getText(),v.getPrenumeCText().getText(),
                    v.getAdresaCText().getText(),v.getTelefonCText().getText(),v.getEmailCText().getText());
            b.addNewPerson(p);
            showOnPersonTable();
            b.storeDataOutput();
        }
    }
    private class deleteCButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            Person p=new Person(new BigInteger(v.getCNPText().getText()),v.getNumeCText().getText(),v.getPrenumeCText().getText(),
                    v.getAdresaCText().getText(),v.getTelefonCText().getText(),v.getEmailCText().getText());
            b.deletePerson(p);
            showOnPersonTable();
            showOnSpendingTable();
            showOnSavingTable();
            b.storeDataOutput();
        }
    }
    private class updateCButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            Person p=new Person(new BigInteger(v.getCNPText().getText()),v.getNumeCText().getText(),v.getPrenumeCText().getText(),
                    v.getAdresaCText().getText(),v.getTelefonCText().getText(),v.getEmailCText().getText());
            b.editPerson(p,v.getNumeCText().getText(),v.getPrenumeCText().getText(),
                    v.getAdresaCText().getText(),v.getTelefonCText().getText(),v.getEmailCText().getText());
            showOnPersonTable();
            b.storeDataOutput();
        }
    }
    private class displaySpendButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            showOnSpendingTable();
        }
    }
    private class insertSpendCButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSpendOpDateText().getText();
            Date d= new Date(s);
             Spending spending=new Spending(new BigInteger(v.getSpendAcNumberText().getText()),Double.parseDouble(v.getSpendCurBalText().getText()),
                    d,v.getSpendCurText().getText());
             b.addNewSpending(spending,new BigInteger(v.getCNPSpendText().getText()));
            showOnSpendingTable();
            b.storeDataOutput();
        }
    }
    private class deleteSpendButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSpendOpDateText().getText();
            Date d= new Date(s);
            Spending spending=new Spending(new BigInteger(v.getSpendAcNumberText().getText()),Double.parseDouble(v.getSpendCurBalText().getText()),
                    d,v.getSpendCurText().getText());
            b.deleteSpending(spending,new BigInteger(v.getCNPSpendText().getText()));
            showOnSpendingTable();
            b.storeDataOutput();
        }
    }
    private class updateSpendButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSpendOpDateText().getText();
            Date d= new Date(s);
            Spending spending=new Spending(new BigInteger(v.getSpendAcNumberText().getText()),Double.parseDouble(v.getSpendCurBalText().getText()),
                    d,v.getSpendCurText().getText());
            b.editSpending(spending,Double.parseDouble(v.getSpendCurBalText().getText()),
                    d,v.getSpendCurText().getText(),new BigInteger(v.getCNPSpendText().getText()));
            showOnSpendingTable();
            b.storeDataOutput();
        }
    }
    private class displaySavButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
           showOnSavingTable();
        }
    }
    private class insertSavButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSavOpDateText().getText();
            Date d= new Date(s);
            Saving saving= new Saving(Double.parseDouble(v.getSavInterestText().getText()),new BigInteger(v.getSavAcNumberText().getText()),
                    Double.parseDouble(v.getSavCurBalText().getText()), d,v.getSavCurText().getText());
            b.addNewSaving(saving,new BigInteger(v.getCNPSavText().getText()));
            showOnSavingTable();
            b.storeDataOutput();
        }
    }
    private class deleteSavButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSavOpDateText().getText();
            Date d= new Date(s);
            Saving saving= new Saving(Double.parseDouble(v.getSavInterestText().getText()),new BigInteger(v.getSavAcNumberText().getText()),
                    Double.parseDouble(v.getSavCurBalText().getText()), d,v.getSavCurText().getText());
            b.deleteSaving(saving,new BigInteger(v.getCNPSavText().getText()));
            showOnSavingTable();
            b.storeDataOutput();
        }
    }
    private class updateSavButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSavOpDateText().getText();
            Date d= new Date(s);
            Saving saving= new Saving(Double.parseDouble(v.getSavInterestText().getText()),new BigInteger(v.getSavAcNumberText().getText()),
                    Double.parseDouble(v.getSavCurBalText().getText()), d,v.getSavCurText().getText());
            b.editSaving(saving,Double.parseDouble(v.getSavInterestText().getText()), Double.parseDouble(v.getSavCurBalText().getText()),
                    d,v.getSavCurText().getText(),new BigInteger(v.getCNPSavText().getText()));
            showOnSavingTable();
            b.storeDataOutput();
        }
    }
    private class addSavButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSavOpDateText().getText();
            Date d= new Date(s);
            Saving saving= new Saving(Double.parseDouble(v.getSavInterestText().getText()),new BigInteger(v.getSavAcNumberText().getText()),
                    Double.parseDouble(v.getSavCurBalText().getText()), d,v.getSavCurText().getText());
            b.writeAccount(saving,Double.parseDouble(v.getBalanceSavText().getText()),new BigInteger(v.getCNPSavText().getText()));
            showOnSavingTable();
            b.storeDataOutput();
        }
    }
    private class addSpendButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSpendOpDateText().getText();
            Date d= new Date(s);
            Spending spending=new Spending(new BigInteger(v.getSpendAcNumberText().getText()),Double.parseDouble(v.getSpendCurBalText().getText()),
                    d,v.getSpendCurText().getText());
            b.writeAccount(spending,Double.parseDouble(v.getBalanceSpendText().getText()),new BigInteger(v.getCNPSpendText().getText()));
            showOnSpendingTable();
            b.storeDataOutput();
        }
    }
    private class withdrawSavButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSavOpDateText().getText();
            Date d= new Date(s);
            Saving saving= new Saving(Double.parseDouble(v.getSavInterestText().getText()),new BigInteger(v.getSavAcNumberText().getText()),
                    Double.parseDouble(v.getSavCurBalText().getText()), d,v.getSavCurText().getText());
            b.readAccount(saving,1,new BigInteger(v.getCNPSavText().getText()));
            showOnSavingTable();
            b.storeDataOutput();
        }
    }
    private class withdrawSpendButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent ev) {
            String s=v.getSpendOpDateText().getText();
            Date d= new Date(s);
            Spending spending=new Spending(new BigInteger(v.getSpendAcNumberText().getText()),Double.parseDouble(v.getSpendCurBalText().getText()),
                    d,v.getSpendCurText().getText());
            b.readAccount(spending,Double.parseDouble(v.getBalanceSpendText().getText()),new BigInteger(v.getCNPSpendText().getText()));
            showOnSpendingTable();
            b.storeDataOutput();
        }
    }
    private void showOnSavingTable(){
        HashMap<BigInteger,ArrayList<Account>> accounts=b.getAccounts();
        String[] columnName= new String[6];
        columnName[0]="AccountNO";
        columnName[1]="Balance";
        columnName[2]="OpeningDate";
        columnName[3]="Currency";
        columnName[4]="InterestRate";
        columnName[5]="OwnerCNP";
        DefaultTableModel model = new DefaultTableModel(columnName,0);
        for(BigInteger p: accounts.keySet()){
            String key=p+"";
            ArrayList<Account> value= accounts.get(p);
            Saving s;
            for(Account a: value ){
                if(a instanceof Saving) {
                    s = (Saving) a;
                    Object [] row= new Object[6];
                    row[0]=a.getAccountNumber();
                    row[1]=a.getCurrentBalance();
                    Format formatter = new SimpleDateFormat("dd/MM/yy");
                    String string = formatter.format(s.getOpeningDate());
                    row[2]=string;
                    row[3]=a.getValuta();
                    row[4]=s.getInterestRate();
                    row[5]=key;
                    model.addRow(row);
                }
            }
        }
        JTable table = new JTable(model);
        v.setSavTable(table);
    }

    private void showOnSpendingTable(){
        HashMap<BigInteger,ArrayList<Account>> accounts=b.getAccounts();
        String[] columnName= new String[5];
        columnName[0]="AccountNO";
        columnName[1]="Balance";
        columnName[2]="OpeningDate";
        columnName[3]="Currency";
        columnName[4]="OwnerCNP";
        DefaultTableModel model = new DefaultTableModel(columnName,0);
        for(BigInteger p: accounts.keySet()){
            String key=p+"";
            ArrayList<Account> value= accounts.get(p);
            Spending s;
            for(Account a: value ){
                if(a instanceof Spending) {
                    s = (Spending) a;
                    Object [] row= new Object[5];
                    row[0]=a.getAccountNumber();
                    row[1]=a.getCurrentBalance();
                    Format formatter = new SimpleDateFormat("dd/MM/yy");
                    String string = formatter.format(s.getOpeningDate());
                    row[2]=string;
                    row[3]=a.getValuta();
                    row[4]=key;
                    model.addRow(row);
                }
            }
        }
        JTable table = new JTable(model);
        v.setSpendTable(table);
    }

    private void showOnPersonTable(){
        String[] columnName = new String[6];
        columnName[0]="CNP";
        columnName[1]="Nume";
        columnName[2]="Prenume";
        columnName[3]="Adresa";
        columnName[4]="Telefon";
        columnName[5]="Email";
        DefaultTableModel model = new DefaultTableModel(columnName,0);
        HashMap<BigInteger, Person> persons = b.getPersons();
        Object[] row= new Object[6];
        for (BigInteger p : persons.keySet()) {
            String key = p + "";
            Person value = persons.get(p);
            row[0] = value.getCNP()+"";
            row[1] = value.getNume()+"";
            row[2] = value.getPrenume();
            row[3] = value.getAdresa();
            row[4] = value.getTelefon();
            row[5] = value.getEmail();
            model.addRow(row);
        }
        JTable table = new JTable(model);
        v.setPTable(table);
    }


}


