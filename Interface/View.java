package Interface;

import javafx.scene.control.Tab;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class View extends JFrame {
    private JFrame frame= new JFrame();
    private JPanel savingPanel= new JPanel();
    private JPanel spendingPanel= new JPanel();
    private JPanel panelClienti=new JPanel();
    private JTabbedPane tabbedPane=new JTabbedPane();


    private JLabel CNP = new JLabel("CNP:");
    private JLabel numeC= new JLabel("Nume:");
    private JLabel prenumeC= new JLabel("Prenume:");
    private JLabel adresaC= new JLabel("Adresa:");
    private JLabel telefonC= new JLabel("Telefon:");
    private JLabel emailC= new JLabel("Email:");
    private JTextField CNPText = new JTextField();
    private JTextField numeCText = new JTextField();
    private JTextField prenumeCText= new JTextField();
    private JTextField adresaCText= new JTextField();
    private JTextField telefonCText= new JTextField();
    private JTextField emailCText=new JTextField();
    private JButton displayC= new JButton("Display Data");
    private JButton insertC= new JButton("INSERT");
    private JButton deleteC= new JButton("DELETE");
    private JButton updateC= new JButton("UPDATE");

    private JLabel savAcNumber= new JLabel("Account NO:");
    private JLabel savCurBal= new JLabel("Balance:");
    private JLabel savInterest= new JLabel("Interest Rate:");
    private JLabel savOpDate = new JLabel("Opening date:");
    private JLabel savCur= new JLabel("Currency:");
    private JTextField savAcNumberText = new JTextField();
    private JTextField savCurBalText= new JTextField();
    private JTextField savInterestText= new JTextField();
    private JTextField savOpDateText = new JTextField();
    private JTextField savCurText= new JTextField();
    private JButton displaySav= new JButton("Display Data");
    private JButton insertSav= new JButton("INSERT");
    private JButton deleteSav= new JButton("DELETE");
    private JButton updateSav= new JButton("UPDATE");


    private JLabel spendAcNumber= new JLabel("Account NO:");
    private JLabel spendCurBal= new JLabel("Balance:");
    private JLabel spendOpDate = new JLabel("Opening date:");
    private JLabel spendCur= new JLabel("Currency:");
    private JTextField spendAcNumberText = new JTextField();
    private JTextField spendCurBalText= new JTextField();
    private JTextField spendOpDateText = new JTextField();
    private JTextField spendCurText= new JTextField();
    private JButton displaySpend= new JButton("Display Data");
    private JButton insertSpend= new JButton("INSERT");
    private JButton deleteSpend= new JButton("DELETE");
    private JButton updateSpend= new JButton("UPDATE");

    private JLabel CNPSav= new JLabel("CNP:");
    private JTextField CNPSavText= new JTextField();
    private JLabel balanceSav= new JLabel("Balance:");
    private JTextField balanceSavText= new JTextField();
    private JLabel numeSav= new JLabel("Nume:");
    private JTextField numeSavText= new JTextField();

    private JLabel CNPSpend= new JLabel("CNP:");
    private JTextField CNPSpendText= new JTextField();
    private JLabel balanceSpend= new JLabel("Balance:");
    private JTextField balanceSpendText= new JTextField();
    private JLabel numeSpend= new JLabel("Nume:");
    private JTextField numeSpendText= new JTextField();

    private JButton addSav= new JButton("ADD");
    private JButton withdrawSav= new JButton("Withdraw");
    private JButton addSpend= new JButton("ADD");
    private JButton withdrawSpend= new JButton("Withdraw");

    private JTable pTable=new JTable() ;
    private JTable savTable= new JTable();
    private JTable spendTable= new JTable();

    private JScrollPane scrtable= new JScrollPane();
    private JScrollPane scrtable2= new JScrollPane();
    private JScrollPane scrtable3 = new JScrollPane();


    public View(){
        frame.setTitle("Bank");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        frame.setLocation(350, 100);
        frame.getContentPane().setLayout(null);
        frame.getContentPane().setBackground(Color.decode("#FAEBD7"));
        savingPanel.setLayout(null);
        spendingPanel.setLayout(null);
        panelClienti.setLayout(null);


        addOnSavingPanel();
        addOnSpendingPanel();
        addOnPersonPanel();

        tabbedPane.addTab("Person",panelClienti);
        tabbedPane.addTab("SavingAccount",savingPanel);
        tabbedPane.addTab("SpendingAccount",spendingPanel);
        frame.setContentPane(tabbedPane);

        frame.setVisible(true);


    }

    private void addOnSavingPanel(){
        savAcNumber.setBounds(10,10,100,20);
        savingPanel.add(savAcNumber);
        savAcNumberText.setBounds(100,10,100,20);
        savingPanel.add(savAcNumberText);
        savCurBal.setBounds(10,40,100,20);
        savingPanel.add(savCurBal);
        savCurBalText.setBounds(100,40,100,20);
        savingPanel.add(savCurBalText);
        savInterest.setBounds(10,70,100,20);
        savingPanel.add(savInterest);
        savInterestText.setBounds(100,70,100,20);
        savingPanel.add(savInterestText);
        savCur.setBounds(10,100,100,20);
        savingPanel.add(savCur);
        savCurText.setBounds(100,100,100,20);
        savingPanel.add(savCurText);
        savOpDate.setBounds(10,130,100,20);
        savingPanel.add(savOpDate);
        savOpDateText.setBounds(100,130,100,20);
        savingPanel.add(savOpDateText);
        displaySav.setBounds(20,190,120,20);
        savingPanel.add(displaySav);
        insertSav.setBounds(20,220,100,20);
        savingPanel.add(insertSav);
        updateSav.setBounds(20,250,100,20);
        savingPanel.add(updateSav);
        deleteSav.setBounds(20,280,100,20);
        savingPanel.add(deleteSav);

        CNPSav.setBounds(10,350,100,20);
        savingPanel.add(CNPSav);
        CNPSavText.setBounds(100,350,100,20);
        savingPanel.add(CNPSavText);
        //numeSav.setBounds(10,380,100,20);
        //savingPanel.add(numeSav);
        //numeSavText.setBounds(100,380,100,20);
        //savingPanel.add(numeSavText);
        balanceSav.setBounds(10,410,100,20);
        savingPanel.add(balanceSav);
        balanceSavText.setBounds(100,410,100,20);
        savingPanel.add(balanceSavText);
        addSav.setBounds(20,440,70,20);
        savingPanel.add(addSav);
        withdrawSav.setBounds(110,440,100,20);
        savingPanel.add(withdrawSav);
    }

    private void addOnSpendingPanel(){
        spendAcNumber.setBounds(10,10,100,20);
        spendingPanel.add(spendAcNumber);
        spendAcNumberText.setBounds(100,10,100,20);
        spendingPanel.add(spendAcNumberText);
        spendCurBal.setBounds(10,40,100,20);
        spendingPanel.add(spendCurBal);
        spendCurBalText.setBounds(100,40,100,20);
        spendingPanel.add(spendCurBalText);
        spendCur.setBounds(10,70,100,20);
        spendingPanel.add(spendCur);
        spendCurText.setBounds(100,70,100,20);
        spendingPanel.add(spendCurText);
        spendOpDate.setBounds(10,100,100,20);
        spendingPanel.add(spendOpDate);
        spendOpDateText.setBounds(100,100,100,20);
        spendingPanel.add(spendOpDateText);
        displaySpend.setBounds(20,160,120,20);
        spendingPanel.add(displaySpend);
        insertSpend.setBounds(20,190,100,20);
        spendingPanel.add(insertSpend);
        updateSpend.setBounds(20,220,100,20);
        spendingPanel.add(updateSpend);
        deleteSpend.setBounds(20,250,100,20);
        spendingPanel.add(deleteSpend);

        CNPSpend.setBounds(10,320,100,20);
        spendingPanel.add(CNPSpend);
        CNPSpendText.setBounds(100,320,100,20);
        spendingPanel.add(CNPSpendText);
       // numeSpend.setBounds(10,350,100,20);
       // spendingPanel.add(numeSpend);
       // numeSpendText.setBounds(100,350,100,20);
       // spendingPanel.add(numeSpendText);
        balanceSpend.setBounds(10,380,100,20);
        spendingPanel.add(balanceSpend);
        balanceSpendText.setBounds(100,380,100,20);
        spendingPanel.add(balanceSpendText);
        addSpend.setBounds(20,410,70,20);
        spendingPanel.add(addSpend);
        withdrawSpend.setBounds(110,410,100,20);
        spendingPanel.add(withdrawSpend);
    }

    private void addOnPersonPanel(){
        CNP.setBounds(10,10,100,20);
        panelClienti.add(CNP);
        CNPText.setBounds(100,10,100,20);
        panelClienti.add(CNPText);
        numeC.setBounds(10,40,100,20);
        panelClienti.add(numeC);
        numeCText.setBounds(100,40,100,20);
        panelClienti.add(numeCText);
        prenumeC.setBounds(10,70,100,20);
        panelClienti.add(prenumeC);
        prenumeCText.setBounds(100,70,100,20);
        panelClienti.add(prenumeCText);
        adresaC.setBounds(10,100,100,20);
        panelClienti.add(adresaC);
        adresaCText.setBounds(100,100,100,20);
        panelClienti.add(adresaCText);
        telefonC.setBounds(10,130,100,20);
        panelClienti.add(telefonC);
        telefonCText.setBounds(100,130,100,20);
        panelClienti.add(telefonCText);
        emailC.setBounds(10,160,100,20);
        panelClienti.add(emailC);
        emailCText.setBounds(100,160,100,20);
        panelClienti.add(emailCText);
        displayC.setBounds(500,20,150,20);
        panelClienti.add(displayC);
        insertC.setBounds(250,70,100,20);
        panelClienti.add(insertC);

        deleteC.setBounds(400,70,100,20);
        panelClienti.add(deleteC);

        updateC.setBounds(550,70,100,20);
        panelClienti.add(updateC);
    }

    public void displayCButtonListener(ActionListener ev){displayC.addActionListener(ev);}
    public void insertCButtonListener(ActionListener ev){insertC.addActionListener(ev);}
    public void deleteCButtonListener(ActionListener ev){deleteC.addActionListener(ev);}
    public void updateCButtonListener(ActionListener ev){updateC.addActionListener(ev);}

    public void displaySpendButtonListener(ActionListener ev){displaySpend.addActionListener(ev);}
    public void insertSpendButtonListener(ActionListener ev){insertSpend.addActionListener(ev);}
    public void deleteSpendButtonListener(ActionListener ev){deleteSpend.addActionListener(ev);}
    public void updateSpendButtonListener(ActionListener ev){updateSpend.addActionListener(ev);}

    public void displaySavButtonListener(ActionListener ev){displaySav.addActionListener(ev);}
    public void insertSavButtonListener(ActionListener ev){insertSav.addActionListener(ev);}
    public void deleteSavButtonListener(ActionListener ev){deleteSav.addActionListener(ev);}
    public void updateSavButtonListener(ActionListener ev){updateSav.addActionListener(ev);}

    public void addSavButtonListener(ActionListener ev){addSav.addActionListener(ev);}
    public void addSpendButtonListener(ActionListener ev){addSpend.addActionListener(ev);}
    public void withdrawSavButtonListener(ActionListener ev){withdrawSav.addActionListener(ev);}
    public void withdrawSpendButtonListener(ActionListener ev){withdrawSpend.addActionListener(ev);}
  //  public void pTableMouseListener(MouseListener ev){pTable.addMouseListener(ev);}


   private void pTableMouseClicked() {
        int i = pTable.getSelectedRow();
        TableModel model = pTable.getModel();
        // Display Slected Row In JTexteFields
        CNPText.setText(model.getValueAt(i,0).toString());
        numeCText.setText(model.getValueAt(i,1).toString());
        prenumeCText.setText(model.getValueAt(i,2).toString());
        adresaCText.setText(model.getValueAt(i,3).toString());
        telefonCText.setText(model.getValueAt(i,4).toString());
        emailCText.setText(model.getValueAt(i,5).toString());

    }

    private void savTableMouseClicked(){
        int i= savTable.getSelectedRow();
        TableModel model = savTable.getModel();
        savAcNumberText.setText(model.getValueAt(i,0).toString());
        savCurBalText.setText(model.getValueAt(i,1).toString());
        savInterestText.setText(model.getValueAt(i,4).toString());
        savCurText.setText(model.getValueAt(i,3).toString());
        savOpDateText.setText(model.getValueAt(i,2).toString());
        CNPSavText.setText(model.getValueAt(i,5).toString());
    }

    private void spendTableMouseClicked(){
        int i= spendTable.getSelectedRow();
        TableModel model = spendTable.getModel();
        spendAcNumberText.setText(model.getValueAt(i,0).toString());
        spendCurBalText.setText(model.getValueAt(i,1).toString());
        spendCurText.setText(model.getValueAt(i,3).toString());
        spendOpDateText.setText(model.getValueAt(i,2).toString());
        CNPSpendText.setText(model.getValueAt(i,4).toString());
    }


    public JTextField getCNPText() {
        return CNPText;
    }

    public JTextField getNumeCText() {
        return numeCText;
    }

    public JTextField getPrenumeCText() {
        return prenumeCText;
    }

    public JTextField getAdresaCText() {
        return adresaCText;
    }

    public JTextField getTelefonCText() {
        return telefonCText;
    }

    public JTextField getEmailCText() {
        return emailCText;
    }

    public JTextField getSavAcNumberText() {
        return savAcNumberText;
    }

    public JTextField getSavCurBalText() {
        return savCurBalText;
    }

    public JTextField getSavInterestText() {
        return savInterestText;
    }

    public JTextField getSavOpDateText() {
        return savOpDateText;
    }

    public JTextField getSavCurText() {
        return savCurText;
    }

    public JTextField getSpendAcNumberText() {
        return spendAcNumberText;
    }

    public JTextField getSpendCurBalText() {
        return spendCurBalText;
    }

    public JTextField getSpendOpDateText() {
        return spendOpDateText;
    }

    public JTextField getSpendCurText() {
        return spendCurText;
    }

    public JTextField getCNPSavText() {
        return CNPSavText;
    }

    public JTextField getBalanceSavText() {
        return balanceSavText;
    }

    public JTextField getNumeSavText() {
        return numeSavText;
    }

    public JTextField getCNPSpendText() {
        return CNPSpendText;
    }

    public JTextField getBalanceSpendText() {
        return balanceSpendText;
    }

    public JTextField getNumeSpendText() {
        return numeSpendText;
    }

    public JTable getpTable() {
        return pTable;
    }

    public JTable getSavTable() {
        return savTable;
    }

    public JTable getSpendTable() {
        return spendTable;
    }

    public void setPTable(JTable table){

        pTable=table;
        scrtable.setViewportView(pTable);
        scrtable.setBounds(20,220,750,300);
        pTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent event) {
                pTableMouseClicked();
            }
        });
        panelClienti.add(scrtable);

    }

    public void setSavTable(JTable table){
        this.savTable=table;
        scrtable2.setViewportView(savTable);
        scrtable2.setBounds(230,20,530,500);
        savTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent event) {
                savTableMouseClicked();
            }
        });
        savingPanel.add(scrtable2);
    }
    public void setSpendTable(JTable table){
        this.spendTable=table;
        scrtable3.setViewportView(spendTable);
        scrtable3.setBounds(230,20,530,500);
        spendTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent event) {
                spendTableMouseClicked();
            }
        });
        spendingPanel.add(scrtable3);
    }
}
