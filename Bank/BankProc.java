package Bank;

import Person.Person;
import Account.Spending;
import Account.Saving;
import Account.Account;

import java.math.BigInteger;
import java.util.Date;

public interface BankProc {
    /**
     * @pre findSaving(accSaving)==null
     * @pre findPerson(CNP)!=null
     * @pre accSaving.getCurrentBalance()!=0
     * @post findSaving(accSaving)!=null
     */
     public void addNewSaving(Saving accSaving,BigInteger CNP);

    /**
     * @pre findSaving(accSaving)!=null
     */
     public void editSaving(Saving accSaving, double interestRate, double currentBalance, Date openingDate, String valuta, BigInteger CNP);

    /**
     * @pre findSaving(accSaving)!=null
     * @post findSaving(accSaving)==null
     */
     public void deleteSaving(Saving accSaving,BigInteger CNP);

    /**
     * @pre findSpending(accSpending)==null
     * @pre findPerson(CNP)!=null
     * @post findSpending(accSpending)!=null
     */
     public void addNewSpending(Spending accSpending,BigInteger CNP);

    /**
     * @pre findSpending(accSpending)!=null
     */
     public void editSpending(Spending accSpending, double currentBalance, Date openingDate, String valuta,BigInteger CNP);

    /**
     * @pre findSpending(accSpending)!=null
     * @post findSpending(accSpending)==null
     */
     public void deleteSpending(Spending accSpending,BigInteger CNP);

    /**
     * @pre findPerson(pers.getCNP())==null
     * @post findPerson(pers.getCNP())!=null
     */
     public void addNewPerson(Person pers);

    /**
     * @pre findPerson(pers.getCNP())!=null
     */
     public void editPerson(Person pers,String nume,String prenume,String adresa,String telefon, String email);

    /**
     * @pre findPerson(pers.getCNP())!=null
     * @post findPerson(pers.getCNP())==null
     */
     public void deletePerson(Person pers);

     public Saving findSaving(Saving accSaving);
     public Spending findSpending(Spending accSpending);
     public BigInteger findPerson(BigInteger CNP);

     public void readAccount(Account a,double balance,BigInteger CNP);

     public void writeAccount(Account a,double balance,BigInteger CNP);
}
