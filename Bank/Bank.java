package Bank;

import Account.Account;
import Account.Saving;
import Account.Spending;
import Observer.Observer;
import Person.Person;
import Observer.Subiect;
import javafx.beans.InvalidationListener;


import java.io.*;
import java.math.BigInteger;
import java.util.*;
public class Bank implements Subiect,BankProc {

    private ArrayList<Observer> obs;
    private HashMap<BigInteger,Person> persons;
    private HashMap<BigInteger,ArrayList<Account>> accounts;
    private ArrayList<Account> acc;
    private String msg=new String();
    public Bank( HashMap<BigInteger,ArrayList<Account>> accounts,HashMap<BigInteger,Person> persons){
        obs=new ArrayList<Observer>();
        this.accounts=accounts;
        this.persons=persons;
    }

    public Bank(){
        obs=new ArrayList<Observer>();
    }

    /**
     * @pre findSaving(accSaving)==null
     * @pre findPerson(CNP)!=null
     * @pre accSaving.getCurrentBalance()!=0
     * @post findSaving(accSaving)!=null
     */
    public void addNewSaving(Saving accSaving,BigInteger CNP){
        assert (findSaving(accSaving)==null);
        assert (findPerson(CNP)!=null);
        assert (accSaving.getCurrentBalance()!=0);
                acc = accounts.getOrDefault(CNP,new ArrayList<Account>());
                acc.add(accSaving);
                accounts.put(CNP, acc);
                Person p=persons.get(CNP);
                msg=p.getNume()+" "+p.getPrenume()+" - S-a adaugat un nou cont cu numarul "+accSaving.getAccountNumber()+".";
                notifyObserver(p);
        assert (findSaving(accSaving)!=null);
    }
    /**
     * @pre findSaving(accSaving)!=null
     */
    public void editSaving(Saving accSaving, double interestRate, double currentBalance, Date openingDate, String valuta,BigInteger CNP){
        assert  (findSaving(accSaving)!=null);
        for(BigInteger a:accounts.keySet()){
            String key=a+"";
            acc=accounts.get(a);
            for(Account s: acc){
                if(s.equals(accSaving)){
                    accSaving= (Saving) s;
                    accSaving.setCurrentBalance(currentBalance);
                    accSaving.setInterestRate(interestRate);
                    accSaving.setOpeningDate(openingDate);
                    accSaving.setValuta(valuta);
                    Person p=persons.get(CNP);
                    msg=p.getNume()+" "+p.getPrenume()+" - Contul cu numarul "+accSaving.getAccountNumber()+" a fost modificat.";
                    notifyObserver(p);
                }
            }
        }
    }
    /**
     * @pre findSaving(accSaving)!=null
     * @post findSaving(accSaving)==null
     */
    public void deleteSaving(Saving accSaving,BigInteger CNP){
        assert (findSaving(accSaving)!=null);
        acc=accounts.getOrDefault(CNP,new ArrayList<Account>());
        Iterator<Account> i=acc.iterator();
        while(i.hasNext()){
            Account a= i.next();
            if(a.equals(accSaving)){
                Person p=persons.get(CNP);
                msg=p.getNume()+" "+p.getPrenume()+" - Contul cu numarul "+a.getAccountNumber()+" a fost sters.";
                notifyObserver(p);
                i.remove();
            }
        }
        assert (findSaving(accSaving)==null);
    }
    /**
     * @pre findSpending(accSpending)==null
     * @pre findPerson(CNP)!=null
     * @post findSpending(accSpending)!=null
     */
    public void addNewSpending(Spending accSpending,BigInteger CNP){

        assert (findSpending(accSpending)==null);
        assert (findPerson(CNP)!=null);
            acc = accounts.getOrDefault(CNP,new ArrayList<Account>());
            acc.add(accSpending);
            accounts.put(CNP, acc);
        Person p=persons.get(CNP);
        msg=p.getNume()+" "+p.getPrenume()+" - S-a adaugat un nou cont cu numarul "+accSpending.getAccountNumber()+".";
        notifyObserver(p);
        assert (findSpending(accSpending)!=null);
    }
    /**
     * @pre findSpending(accSpending)!=null
     */
    public void editSpending(Spending accSpending, double currentBalance, Date openingDate, String valuta,BigInteger CNP){
        assert (findSpending(accSpending)!=null);
        for(BigInteger a:accounts.keySet()){
            String key=a+"";
            acc=accounts.get(a);
            for(Account s: acc){
                if(s.equals(accSpending)){
                    s.setCurrentBalance(currentBalance);
                    s.setOpeningDate(openingDate);
                    s.setValuta(valuta);
                    Person p=persons.get(CNP);
                    msg=p.getNume()+" "+p.getPrenume()+" - Contul cu numarul "+accSpending.getAccountNumber()+" a fost modificat.";
                    notifyObserver(p);
                }
            }
        }
    }
    /**
     * @pre findSpending(accSpending)!=null
     * @post findSpending(accSpending)==null
     */
    public void deleteSpending(Spending accSpending,BigInteger CNP){
        assert (findSpending(accSpending)!=null);
        acc=accounts.getOrDefault(CNP,new ArrayList<Account>());
        Iterator<Account> i=acc.iterator();
        while(i.hasNext()){
            Account a= i.next();
            if(a.equals(accSpending)){
                Person p=persons.get(CNP);
                msg=p.getNume()+" "+p.getPrenume()+" - Contul cu numarul "+a.getAccountNumber()+" a fost sters.";
                notifyObserver(p);
                i.remove();
            }
        }
        assert (findSpending(accSpending)==null);
    }
    /**
     * @pre findPerson(pers.getCNP())==null
     * @post findPerson(pers.getCNP())!=null
     */
    public void addNewPerson(Person pers){
        assert (findPerson(pers.getCNP())==null);
        BigInteger CNP=pers.getCNP();
        persons.put(CNP,pers);
        register(pers);
        assert (findPerson(pers.getCNP())!=null);
    }
    /**
     * @pre findPerson(pers.getCNP())!=null
     */
    public void editPerson(Person pers,String nume,String prenume,String adresa,String telefon, String email){
        assert (findPerson(pers.getCNP())!=null);
        BigInteger CNP=pers.getCNP();
        pers=persons.get(CNP);
        pers.setAdresa(adresa);
        pers.setEmail(email);
        pers.setNume(nume);
        pers.setPrenume(prenume);
        pers.setTelefon(telefon);

    }
    /**
     * @pre findPerson(pers.getCNP())!=null
     * @post findPerson(pers.getCNP())==null
     */
    public void deletePerson(Person pers){
        assert (findPerson(pers.getCNP())!=null);
        persons.remove(pers.getCNP());
        accounts.remove(pers.getCNP());
        unregister(pers);
        assert (findPerson(pers.getCNP())==null);
    }

    public void printAllPersons(){
        for(BigInteger p: persons.keySet()){
            String key=p+"";
            String value= persons.get(p).toString();
            System.out.println(key+ "  ----- "+value);
        }
    }

    public void printAllAccounts(){
       for(BigInteger p: accounts.keySet()){
            String key=p+"";
            ArrayList<Account> value= accounts.get(p);
            for(Account a: value ){
                System.out.println("CNP ="+p+" "+a);
            }
        }
    }

    public Saving findSaving(Saving accSaving){
        for(BigInteger p: accounts.keySet()){
            String key=p+"";
            ArrayList<Account> value= accounts.get(p);
            for(Account a: value ){
                if(a.equals(accSaving))
                    return (Saving) a;
            }
        }
        return null;
    }

    public Spending findSpending(Spending accSpending){
        for(BigInteger p: accounts.keySet()){
            String key=p+"";
            ArrayList<Account> value= accounts.get(p);
            for(Account a: value ){
                if(a.equals(accSpending))
                    return (Spending) a;
            }
        }
        return null;
    }
    public BigInteger findPerson(BigInteger CNP){
        for(BigInteger p: persons.keySet()){
            String key=p+"";
            String value= persons.get(p).toString();
            if(p.equals(CNP)){
                return CNP;
            }
        }
        return null;
    }

    @Override
    public void readAccount(Account a,double balance,BigInteger CNP) {
        if(a instanceof Saving){
            double p=findSaving((Saving)a).getPlus();
            double b=findSaving((Saving)a).withdraw();
            msg=persons.get(CNP).getNume()+" "+persons.get(CNP).getPrenume()+" - Din contul de econimii "+a.getAccountNumber()+" au fost retrasi "+b+" "
                    +a.getValuta()+". Au fost adaugati la suma depusa "+p+" "+
                    a.getValuta()+". Au ramas "+findSaving((Saving)a).getCurrentBalance()+ " "+a.getValuta()+".";
            notifyObserver(persons.get(CNP));
        }
        if( a instanceof Spending){
            findSpending((Spending) a).withdraw(balance);
            msg=persons.get(CNP).getNume()+" "+persons.get(CNP).getPrenume()+" - Din contul "+ a.getAccountNumber()+" au fost retrasi "+balance+" "+
                    a.getValuta()+". Au mai ramas "+ findSpending((Spending) a).getCurrentBalance()+ " "+a.getValuta();
            notifyObserver(persons.get(CNP));
        }
    }

    @Override
    public void writeAccount(Account a,double balance,BigInteger CNP) {
        if(a instanceof Saving){
            if(findSaving((Saving)a).getOk()==0) {
                findSaving((Saving)a).add(balance);
                msg = persons.get(CNP).getNume() + " " + persons.get(CNP).getPrenume() + " - In contul de econimii " + a.getAccountNumber()
                        + " au fost adaugati " +balance + " "
                        + a.getValuta() + ". Suma curenta este "
                        + findSaving((Saving)a).getCurrentBalance() + " " + a.getValuta() + ".";
                notifyObserver(persons.get(CNP));
            }
            else
            {
                System.out.println("In acest cont sunt deja adaugati bani("+findSaving((Saving)a).getCurrentBalance()+")."+
                        " Creati un cont nou de economii sau retrageti banii din cont.");
            }
        }
        if( a instanceof Spending){
            findSpending((Spending) a).add(balance);
            msg=persons.get(CNP).getNume() + " " + persons.get(CNP).getPrenume() + " - In contul " + a.getAccountNumber()
                    + " au fost adaugati " +balance + " "
                    + a.getValuta() + ". Suma curenta este "
                    +  findSpending((Spending) a).getCurrentBalance() + " " + a.getValuta() + ".";
            notifyObserver(persons.get(CNP));
        }
    }
    public  void storeDataOutput(){
        try
        {
            FileOutputStream fs=new FileOutputStream("Accounts.ser");
            ObjectOutputStream os=new ObjectOutputStream(fs);
           os.writeObject(accounts);
            os.close();
            fs.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        try
        {
            FileOutputStream fs=new FileOutputStream("Persons.ser");
            ObjectOutputStream os=new ObjectOutputStream(fs);
           os.writeObject(persons);
            os.close();
            fs.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public  void storeDataInput() {


        try
        {
            FileInputStream fs=new FileInputStream("Persons.ser");
            ObjectInputStream os=new ObjectInputStream(fs);
            persons= (HashMap<BigInteger, Person>) os.readObject();

            os.close();
            fs.close();
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }

        try
        {
            FileInputStream fs=new FileInputStream("Accounts.ser");
            ObjectInputStream os=new ObjectInputStream(fs);
            accounts.clear();
            accounts= (HashMap<BigInteger, ArrayList<Account>>) os.readObject();

            os.close();
            fs.close();
        } catch (ClassNotFoundException | IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public ArrayList<Observer> getObs() {
        return obs;
    }
    public HashMap<BigInteger, Person> getPersons() {
        return persons;
    }
    public HashMap<BigInteger, ArrayList<Account>> getAccounts() {
        return accounts;
    }
        @Override
        public void unregister(Observer o) {
            int observerIndex=obs.indexOf(o);
            obs.remove(observerIndex);
        }

        @Override
        public void notifyObserver(Observer o) {
               o.update(msg);
        }

        @Override
        public void register(Observer o) {
            obs.add(o);
        }
    @Override
    public void addListener(InvalidationListener listener) { }
    @Override
    public void removeListener(InvalidationListener listener) { }
}

