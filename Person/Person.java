package Person;

import Observer.Observer;
import javafx.beans.Observable;

import java.math.BigInteger;
import java.util.Objects;

public class Person implements java.io.Serializable,Observer{

    private BigInteger CNP;
    private String nume;
    private String prenume;
    private String adresa;
    private String telefon;
    private String email;

    public Person(BigInteger CNP,String nume,String prenume,String adresa,String telefon, String email){
        this.CNP=CNP;
        this.nume=nume;
        this.prenume=prenume;
        this.adresa=adresa;
        this.telefon=telefon;
        this.email=email;
    }

    public Person(){

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return CNP == person.CNP;
    }

    @Override
    public int hashCode() {

        return Objects.hash(CNP);
    }

    public BigInteger getCNP() {
        return CNP;
    }

    public String getNume() {
        return nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public String getAdresa() {
        return adresa;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setNume(String nume){
        this.nume=nume;
    }

    public void setPrenume(String prenume){
        this.prenume=prenume;
    }

    public void setAdresa(String adresa){
        this.adresa=adresa;
    }

    public void setTelefon(String telefon){
        this.telefon=telefon;
    }
    public  void setEmail(String email){
        this.email=email;
    }

    public String toString(){
        return "Client [CNP="+ CNP+" nume="+nume+ " prenume="+prenume+" adresa="+adresa+" telefon="+telefon+ " email="+ email;
    }

    @Override
    public void update( String msg) {
        System.out.println(msg);
    }

}
