package Observer;

import javafx.beans.Observable;

public interface Subiect extends Observable{
    public void register(Observer o);
    public void unregister(Observer o);
    public void notifyObserver(Observer o);
}
